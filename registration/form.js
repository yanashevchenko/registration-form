let nameField = document.querySelector('#first-name');
let nameFieldStar = document.querySelector('.input-group-firstname');

let lastnameField = document.querySelector('#last-name');
let lastnameFieldStar = document.querySelector('.input-group-lastname');

let emailField = document.querySelector('#email');
let emailFieldStar = document.querySelector('.input-group-email');

let passwordField = document.querySelector('#password');
let passwordFieldStar = document.querySelector('.input-group-password');

let confirmPasswordField = document.querySelector('#confirm-password');
let confirmPasswordFieldStar = document.querySelector('.input-group-confirmpassword');

let phoneField = document.querySelector('#phone');
let phoneFieldStar = document.querySelector('.dropdown-group-phone');

let streetField = document.querySelector('#street-address');
let streetFieldStar = document.querySelector('.input-group-street');

let countryCodeField = document.querySelector('#countrycode');
let countryCodeFieldStar = document.querySelector('.dropdown-group-countrycode');

let countryField = document.querySelector('#country');
let countryFieldStar = document.querySelector('.dropdown-group-country');

let cityFieldStar = document.querySelector('.dropdown-group-city');

let zipField = document.querySelector('#zip-code');
let zipFieldStar = document.querySelector('.input-group-zipcode');

nameField.addEventListener('keyup', function() {
    nameFieldStar.setAttribute('class', 'hideStar');
});

lastnameField.addEventListener('keyup', function() {
    lastnameFieldStar.setAttribute('class', 'hideStar');
});

emailField.addEventListener('keyup', function() {
    emailFieldStar.setAttribute('class', 'hideStar');
});

passwordField.addEventListener('keyup', function() {
    passwordFieldStar.setAttribute('class', 'hideStar');
});

confirmPasswordField.addEventListener('keyup', function() {
    confirmPasswordFieldStar.setAttribute('class', 'hideStar');
});

countryCodeField.addEventListener('click', function() {
    countryCodeFieldStar.setAttribute('class', 'hideStar');
});

phoneField.addEventListener('keyup', function() {
    phoneFieldStar.setAttribute('class', 'hideStar');
});

streetField.addEventListener('keyup', function() {
    streetFieldStar.setAttribute('class', 'hideStar');
});

countryField.addEventListener('click', function() {
    countryFieldStar.setAttribute('class', 'hideStar');
    cityFieldStar.setAttribute('class', 'hideStar');
});

zipField.addEventListener('keyup', function() {
    zipFieldStar.setAttribute('class', 'hideStar');
});

let customSelect = document.getElementsByClassName('custom-select');

for (let i = 0; i < customSelect.length; i++) {
  let selectElement = customSelect[i].getElementsByTagName('select')[0];
  let selectedItemDiv = document.createElement('div');
  selectedItemDiv.setAttribute('class', 'select-selected');
  selectedItemDiv.innerHTML = selectElement.options[selectElement.selectedIndex].innerHTML;
  customSelect[i].appendChild(selectedItemDiv);

  let optionListDiv = document.createElement('div');
  optionListDiv.setAttribute('class', 'select-items select-hide');
  for (let j = 1; j < selectElement.length; j++) {
    let optionItemDiv = document.createElement('div');
    optionItemDiv.innerHTML = selectElement.options[j].innerHTML;
    
    optionItemDiv.addEventListener('click', function(e) {
        let updateSelectedItem, sameAsSelected;
        let updateSelect = this.parentNode.parentNode.getElementsByTagName('select')[0];
        updateSelectedItem = this.parentNode.previousSibling;
        for (let i = 0; i < updateSelect.length; i++) {
          if (updateSelect.options[i].innerHTML === this.innerHTML) {
            updateSelect.selectedIndex = i;
            updateSelectedItem.innerHTML = this.innerHTML;
            sameAsSelected = this.parentNode.getElementsByClassName('same-as-selected');
            for (let k = 0; k < sameAsSelected.length; k++) {
                sameAsSelected[k].removeAttribute('class');
            }
            this.setAttribute('class', 'same-as-selected');
            break;
          }
        }
        updateSelectedItem.click();
    });
    optionListDiv.appendChild(optionItemDiv);
  }
  customSelect[i].appendChild(optionListDiv);
  
  selectedItemDiv.addEventListener('click', function(e) {
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle('select-hide');
    this.classList.toggle('select-arrow-active');
    });
}

function closeAllSelect(element) {
  let arrNo = [];
  let selectItems = document.getElementsByClassName('select-items');
  let selectSelected = document.getElementsByClassName('select-selected');
  for (let i = 0; i < selectSelected.length; i++) {
    if (element === selectSelected[i]) {
      arrNo.push(i);
    } else {
      selectSelected[i].classList.remove('select-arrow-active');
    }
  }
  for (let i = 0; i < selectItems.length; i++) {
    if (arrNo.indexOf(i)) {
        selectItems[i].classList.add('select-hide');
    }
  }
}

document.addEventListener('click', closeAllSelect);

let countriesSelect = document.querySelector('#country');
let citiesSelect = document.querySelector('#cities');

let data = {
  qatar: ['Al Daayen', 'Al Khor', 'Al Rayyan', 'Doha', 'Shamal'],
  france: ['Paris', 'Marseille', 'Bordeaux', 'Lyon', 'Strasbourg'],
  ukraine: ['Kyiv', 'Kharkiv', 'Lviv', 'Odessa', 'Dnipro'],
  sweden: ['Stockholm', 'Gothenburg', 'Helsingborg', 'Uppsala', 'Lund'],
  norway: ['Bergen', 'Oslo', 'Stavanger', 'Trondheim', 'Alesund'],
  finland: ['Helsinki', 'Tampere', 'Turku', 'Oulu', 'Dorter']
}

countriesSelect.addEventListener('change', function() {
  let cities = data[this.value];
  citiesSelect.length = 0;

  for (let i = 0; i < cities.length; i++) {
    citiesSelect.add(new Option(cities[i]));
  }
});

function verifyFirstname() {
  let firstNameInput = document.getElementById('first-name');
  const firstNameRegex = /^[a-zA-Z]+$/;
  let firstNameMessage = document.getElementById('firstNameMessage');
  if (firstNameInput.value.length === 0 || firstNameInput.value === '') {
    firstNameMessage.style.color = 'red';
    firstNameInput.style.border = '2px solid red';
    firstNameMessage.innerHTML = 'Mandatory field';
    document.getElementById('submit').disabled = true;
  } else if (firstNameRegex.test(firstNameInput.value)) {
    firstNameInput.style.border = '2px solid #f0ecec';
    firstNameMessage.style.color = 'white';
    document.getElementById('submit').disabled = false;
  } else {
    firstNameMessage.style.color = 'red';
    firstNameInput.style.border = '2px solid red';
    firstNameMessage.innerHTML = 'Invalid first name entree';
    document.getElementById('submit').disabled = true;
  }
}

function verifyLastname() {
  let lastNameInput = document.getElementById('last-name');
  const lastNameRegex = /^[a-zA-Z]+$/;
  let lastNameMessage = document.getElementById('lastNameMessage');
  if (lastNameInput.value.length === 0 || lastNameInput.value === '') {
    lastNameMessage.style.color = 'red';
    lastNameInput.style.border = '2px solid red';
    lastNameMessage.innerHTML = 'Mandatory field';
    document.getElementById('submit').disabled = true;
  } else if (lastNameRegex.test(lastNameInput.value)) {
    lastNameInput.style.border = '2px solid #f0ecec';
    lastNameMessage.style.color = 'white';
    document.getElementById('submit').disabled = false;
  } else {
    lastNameMessage.style.color = 'red';
    lastNameInput.style.border = '2px solid red';
    lastNameMessage.innerHTML = 'Invalid last name entree';
    document.getElementById('submit').disabled = true;
  }
}

function verifyEmail() {
  let emailInput = document.getElementById('email');
  let emailValidationMessage = document.getElementById('emailValidationMessage');
  const emailRegex = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/;
  if (emailInput.value.length === 0 || emailInput.value === '') {
    emailValidationMessage.style.color = 'red';
    emailInput.style.border = '2px solid red';
    emailValidationMessage.innerHTML = 'Mandatory field';
    document.getElementById('submit').disabled = true;
  } else if (emailRegex.test(emailInput.value)) {
    emailInput.style.border = '2px solid #f0ecec';
    emailValidationMessage.style.color = 'white';
    document.getElementById('submit').disabled = false;
  } else {
    emailValidationMessage.style.color = 'red';
    emailInput.style.border = '2px solid red';
    emailValidationMessage.innerHTML = 'Invalid email entree';
    document.getElementById('submit').disabled = true;
  }
}

function passwordChanged() {
    let strength = document.getElementById('strength');
    let strongRegex = new RegExp('^(?=.{14,})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*\\W).*$', 'g');
    let mediumRegex = 
    new RegExp('^(?=.{10,})(((?=.*[A-Z])(?=.*[a-z]))|((?=.*[A-Z])(?=.*[0-9]))|((?=.*[a-z])(?=.*[0-9]))).*$', 'g');
    let enoughRegex = new RegExp('(?=.{8,}).*', 'g');
    let pwd = document.getElementById('password');
    if (pwd.value.length === 0) {
        strength.innerHTML = 'Type Password';
    } else if (false === enoughRegex.test(pwd.value)) {
        strength.innerHTML = 'More Characters';
    } else if (strongRegex.test(pwd.value)) {
        strength.innerHTML = '<span style="color:green">Password Strength: Strong</span>';
    } else if (mediumRegex.test(pwd.value)) {
        strength.innerHTML = '<span style="color:orange">Password Strength: Medium</span>';
    } else {
        strength.innerHTML = '<span style="color:red">Password Strength: Weak</span>';
    }
}

function verifyPassword() {
  let pwd = document.getElementById('password');
  let confirmPassword = document.getElementById('confirm-password');
  let confirmPasswordMessage = document.getElementById('confirmPasswordMessage');
  if (pwd.value === confirmPassword.value) {
    confirmPasswordMessage.style.color = 'green';
    confirmPasswordMessage.innerHTML = 'Passwords match';
    document.getElementById('submit').disabled = false;
  } else {
    confirmPasswordMessage.style.color = 'red';
    confirmPasswordMessage.innerHTML = 'The password doesn\'t match';
    document.getElementById('submit').disabled = true;
  }
}

function isCountrySelected() {
  let countrySelection = document.getElementById('country');
  let countryValidationMessage = document.getElementById('countryValidationMessage');
  if (countrySelection.value === 0) {
    countryValidationMessage.style.color = 'red';
    countrySelection.style.border = '2px solid red';
    countryValidationMessage.innerHTML = 'Please select your country';
    document.getElementById('submit').disabled = true;
  }
}

function verifyPhone() {
  let phoneInput = document.getElementById('phone');
  let phoneValidationMessage = document.getElementById('phoneValidationMessage');
  const phoneRegex = /^\d{9}$/;
  phoneInput.value = phoneInput.value.replace(/[\s\-]/g, '');
  if (phoneInput.value.length === 0 || phoneInput.value === '') {
    phoneValidationMessage.style.color = 'red';
    phoneInput.style.border = '2px solid red';
    phoneValidationMessage.innerHTML = 'Mandatory field';
    document.getElementById('submit').disabled = true;
  } else if (phoneRegex.test(phoneInput.value)) {
    phoneInput.style.border = '2px solid #f0ecec';
    phoneValidationMessage.style.color = 'white';
    document.getElementById('submit').disabled = false;
  } else {
    phoneValidationMessage.style.color = 'red';
    phoneInput.style.border = '2px solid red';
    phoneValidationMessage.innerHTML = 'Must be 9 digits long. Make sure to select country code first';
    document.getElementById('submit').disabled = true;
  }
}

function verifyStreet() {
  let streetInput = document.getElementById('street-address');
  let streetValidationMessage = document.getElementById('streetValidationMessage');
  const streetRegex = /^\s*\S+(?:\s+\S+){2}/;
  if (streetInput.value.length === 0 || streetInput.value === '') {
    streetValidationMessage.style.color = 'red';
    streetInput.style.border = '2px solid red';
    streetValidationMessage.innerHTML = 'Mandatory field';
    document.getElementById('submit').disabled = true;
  } else if (streetRegex.test(streetInput.value)) {
    streetInput.style.border = '2px solid #f0ecec';
    streetValidationMessage.style.color = 'white';
    document.getElementById('submit').disabled = false;
  } else {
    streetValidationMessage.style.color = 'red';
    streetInput.style.border = '2px solid red';
    streetValidationMessage.innerHTML = 'Invalid street address entree';
    document.getElementById('submit').disabled = true;
  }
}

function verifyZipcode() {
  let zipCodeInput = document.getElementById('zip-code');
  let zipCodeValidationMessage = document.getElementById('zipCodeValidationMessage');
  const zipCodeRegex = /^\d{5}(?:[-\s]\d{4})?$/;
  if (zipCodeInput.value.length === 0 || zipCodeInput.value === '') {
    zipCodeValidationMessage.style.color = 'red';
    zipCodeInput.style.border = '2px solid red';
    zipCodeValidationMessage.innerHTML = 'Mandatory field';
    document.getElementById('submit').disabled = true;
  } else if (zipCodeRegex.test(zipCodeInput.value)) {
    zipCodeInput.style.border = '2px solid #f0ecec';
    zipCodeValidationMessage.style.color = 'white';
    document.getElementById('submit').disabled = false;
  } else {
    zipCodeValidationMessage.style.color = 'red';
    zipCodeInput.style.border = '2px solid red';
    zipCodeValidationMessage.innerHTML = 'Invalid zip-code entree';
    document.getElementById('submit').disabled = true;
  }
}

let newslettersContainer = document.createElement('div');
function isSignedUpForNewsletter() {
  if (document.getElementById('subscribe').checked) {
    newslettersContainer.innerHTML = `You are SUBSCRIBED to our weekly newsletter!`;
  } else {
    newslettersContainer.innerHTML = `You aren't subscribed to our newsletter.`;
  }
}

function showInput() {
  let form = document.getElementById('form');
  let displayInputBox = document.createElement('div');
  displayInputBox.setAttribute('class', 'displayInputBox');
  form.after(displayInputBox);

  let accountInformationContainer = document.createElement('div');
  accountInformationContainer.setAttribute('class', 'accountInformationContainer');
  displayInputBox.prepend(accountInformationContainer);

  let contactInformationContainer = document.createElement('div');
  contactInformationContainer.setAttribute('class', 'contactInformationContainer');
  accountInformationContainer.prepend(contactInformationContainer);

  newslettersContainer.setAttribute('class', 'newslettersContainer');
  contactInformationContainer.after(newslettersContainer);
  isSignedUpForNewsletter();

  let accountInformationHeader = document.createElement('h3');
  accountInformationHeader.setAttribute('class', 'account-info');
  displayInputBox.prepend(accountInformationHeader);
  accountInformationHeader.innerHTML = `Account Information`;

  let line = document.createElement('hr');
  accountInformationHeader.after(line);

  let contactInformationHeader = document.createElement('h5');
  contactInformationHeader.setAttribute('class', 'contact-info');
  contactInformationContainer.prepend(contactInformationHeader);
  contactInformationHeader.innerHTML = `Contact Information`;

  let newslettersContainerHeader = document.createElement('h5');
  newslettersContainerHeader.setAttribute('class', 'newsletter-info');
  newslettersContainer.prepend(newslettersContainerHeader);
  newslettersContainerHeader.innerHTML = `Newsletters`;

  let contactInformationNameField = document.createElement('div');
  contactInformationNameField.setAttribute('class', 'name-field');
  contactInformationContainer.append(contactInformationNameField);
  contactInformationNameField.innerHTML = document.getElementById('first-name').value + ' ' +
  document.getElementById('last-name').value;

  let contactInformationEmailField = document.createElement('div');
  contactInformationEmailField.setAttribute('class', 'email-field');
  contactInformationContainer.append(contactInformationEmailField);
  contactInformationEmailField.innerHTML = document.getElementById('email').value;

  let addressBookContainer = document.createElement('div');
  addressBookContainer.setAttribute('class', 'addressBookContainer');
  displayInputBox.append(addressBookContainer);

  let addressBookHeader = document.createElement('h3');
  addressBookHeader.setAttribute('class', 'address-info');
  addressBookContainer.prepend(addressBookHeader);
  addressBookHeader.innerHTML = `Address Book`;

  let addressLine = document.createElement('hr');
  addressBookHeader.after(addressLine);

  let addressBookStreetField = document.createElement('div');
  addressBookStreetField.setAttribute('class', 'street-field');
  addressBookContainer.append(addressBookStreetField);
  addressBookStreetField.innerHTML = document.getElementById('street-address').value + ' ';

  let addressBookCityField = document.createElement('div');
  addressBookCityField.setAttribute('class', 'city-field');
  addressBookContainer.append(addressBookCityField);
  addressBookCityField.innerHTML = document.getElementById('cities').value + ',' + ' ' +
  document.getElementById('zip-code').value;

  let addressBookCountryField = document.createElement('div');
  addressBookCountryField.setAttribute('class', 'country-field');
  addressBookCountryField.setAttribute('id', 'page-bottom');
  addressBookContainer.append(addressBookCountryField);
  addressBookCountryField.innerHTML = document.getElementById('country').value + ' ';
}

document.getElementById('form').addEventListener('submit', (e) => {

  e.preventDefault();

  verifyFirstname();
  verifyLastname();
  verifyEmail();
  passwordChanged();
  verifyPassword();
  verifyPhone();
  isCountrySelected();
  verifyStreet();
  verifyZipcode();

  if (verifyFirstname() === true && 
    verifyLastname() === true && 
    verifyEmail() === true && 
    passwordChanged() === true && 
    verifyPassword() === true && 
    verifyPhone() === true && 
    isCountrySelected() === true && 
    verifyStreet() === true && 
    verifyZipcode() === true) {
    showInput();
    document.getElementById('submit').disabled = false;
  } else {
    document.getElementById('submit').disabled = true;
  }
})